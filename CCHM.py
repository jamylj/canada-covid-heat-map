import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style="darkgrid")


#Read in the Covid Cases per Province data
case_file = "D:\\Python\\CCHM\\provintial_cases.csv"
covid_data = pd.read_csv(case_file)


#Read in the Canada Provincial shape file
shp_file = "D:\\Python\\CCHM\\Canada_Provintial_Shape_File\\canada.shp"
province_regions = gpd.read_file(shp_file)
#Column 2 has the names of each province 

#Show the output of the shape file
#province_regions.plot(figsize=(15,8))
#plt.show()



#Join the shape file and dataset together by province
#Label "PRENAME" (Shape File) and Label "province" (Dataset)
merged_data = province_regions.set_index('PRENAME').join(covid_data.set_index('province'))
merged_data = merged_data.reset_index()


#Set up plot
fig, ax = plt.subplots(1, figsize=(18,8))
ax.axis('off')
ax.set_title('Canada Covid Heat Map', fontdict={'fontsize': '40', 'fontweight' : '3'})

#Set up color ranges
color = 'Oranges'
#Get min and max values from the covid data
min_val = merged_data['cases'].min()
max_val = merged_data['cases'].max()

#Create the scalar map and color bar
sm = plt.cm.ScalarMappable(cmap=color, norm=plt.Normalize(vmin=min_val, vmax=max_val))
sm._A = []
cbar = fig.colorbar(sm)
cbar.ax.tick_params(labelsize=20)


#Merge the color bar and map together
merged_data.plot('cases', cmap=color, linewidth=0.8, ax=ax,  figsize=(18,8)) #column='cases', cmap=color, ax=ax, figsize=(18,8)
plt.show()




